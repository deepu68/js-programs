// closure

function makeadder(a)
{
  return function(b){
    return a+b;
  };
}

var a = makeadder(5);
var b = makeadder(10);
console.log(a(5));         // output = 10




// call method()

const employee = {
    empName : function(salary){
        return this.Name + " salary is " + salary;
    }
}

const emp1 = {
    Name : "Deepu Pandit"
}

console.log(employee.empName.call(emp1,25000));


//  apply method()

const person = {
    fullName: function(city, country) {
      return this.firstName + " " + this.lastName + "," + city + "," + country;
    }
  }
  
  const person1 = {
    firstName:"John",
    lastName: "Doe"
  }
  
  person.fullName.apply(person1, ["Oslo", "Norway"]);

  // in the above code fullName method of person is applied on object person1.


// bind method

const car = {
    speed : 5,
    start : function(){
      console.log('start at ' + this.speed );
    }
  }
  
  const plane = {
    speed : 10,
    fly : function(){
      console.log('fly at ' + this.speed);
    }
  }
  
  let taxiing = car.start.bind(plane);   // Here plane object borrowing the start method from the car object via bind()
  taxiing();


//   the bind(), call(), and apply() methods are also known as borrowing functions.



//  Java Script Prototype

function Employee(name){
  this.name = name;
}


const emp = new Employee('Deepu Pandit');

//  To add a property to object constructor

Employee.prototype.greet = function(){
  return 'Hi, I am '+ this.name;
};

console.log(emp.greet());

// Prototypal inheretence

let person = {
  name : "Deepu Pandit",
  greet: function(){
    return 'Hi, I am '+ this.name;
  }
}

let teacher = {
  teach : function(subject){
    return 'I can teach '+ subject;

  }
}

// To inherit the property of person object in teacher

teacher.__proto__ = person;

console.log(teacher.name);  // Deepu Pandit





//  Function Currying : Evaluating the function with multiple by taking one arguement at a time
// instead of taking all arguements at once

function myFun(a){
    return (b) =>{
        return (c)=>{
            return a*b*c;
        }
    }
}

console.log(myFun(5)(6)(8));     // 240





//  forEach is method calls a function once for each element.

const arr = [2,5,7,8];

arr.forEach(product);

function product(item,index,arr){
    arr[index]=item*10;
}

console.log(arr);   // [20,50,70,80]





// Map is a method creates a new array with results of calling a functon for every array element

const persons = [
    {firstName: "Deepu", lastName: "Pandit"},
    {firstName: "Saksham", lastName: "Pandit"}
]

console.log(persons.map(myfun));   // ['Deepu Pandit' , 'Saksham Pandit']

function myfun(item){
    return [item.firstName, item.lastName].join(" ");
}





//  Reduce is method which executes a reducer function for each value of an array and returns
// a single value which is function accumulated result.

const numbers = [175, 50, 25];

console.log(numbers.reduce(myFunc));        //  100

function myFunc(total, num) {
  return total - num;
}



// JavaScript Promise Object

let myPromise = new Promise(function(myResolve,myReject){
    // producing code

    let x=7;
    if(x%2==0)
    myResolve("even");
    else
    myReject("odd");
});

// consuming code

myPromise.then(
    function(value){
        console.log(value);
    },
    function(error){
        console.log(error);
    }
);

// Async 



async function myFun(){
    return "Hello";
}

myFun().then(
    function(value){
        console.log(value);

    }
)

// Async , Await and Promise

async function myFun(){
    let myPromise = new Promise(function(myResolve,myReject){
        setTimeout(function(){myResolve("Deepu Pandit");},5000);
    
    });

    console.log(await myPromise);
}

myFun();

// Handling multiple promises

var promiseCall = function (waitSecond, returnData) {
    return function (resolve, reject) {
        setTimeout(resolve, waitSecond, returnData);
    };
};
var p1 = new Promise(promiseCall(1000, "one"));
var p2 = new Promise(promiseCall(2000, "two"));
var p3 = new Promise(promiseCall(3000, "three"));
var p4 = new Promise(promiseCall(4000, "four"));
var p5 = new Promise(function (resolve, reject) {
    reject('5th Promise Rejected');
});

// Promise.all(iterable)
// if resolved return array of resolved values
// if rejects, it is returned with the first reason

Promise.all([p1, p2, p3, p4, p5, p6]).then(function (value) {
    console.log(value);
}, function (reason) {
    // Not Called
    console.log(reason);
});

// Promise.allSettled(iterable)
// It returns detailed status of all the promises with their values

Promise.allSettled([p1, p2, p3, p4, p5, p6]).then(function (value) {
    console.log(value);
}, function (reason) {
    // Not Called
    console.log(reason);
});

// Promise.any(iterable) 
// It returns as soon as one of the promise is resolved

Promise.any([p1, p2, p3, p4]).then(function (value) {
    console.log(value);
}, function (reason) {
    // Not Called
    console.log(reason);
});

// Promise.race(iterable) 
// It returns with the new promise which is resolved or rejected first

Promise.race([p1, p2, p3, p4]).then(function (value) {
    console.log(value);
}, function (reason) {
    // Not Called
    console.log(reason);
});